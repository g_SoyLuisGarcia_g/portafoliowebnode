'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.on('/').render('welcome')

Route.get('productos/productoTabla','ProductController.index')//index
Route.get('productos/crearProducto','ProductController.mostrar')//entra a la pagina de crear
Route.post('productos/productoTabla','ProductController.create')//crear el producto
Route.get('productos/modificarProducto/:id','ProductController.viewModificar')//entra a la pagina de modificar
Route.post('productos/modificarProducto/:id','ProductController.update')//modificar el producto
Route.get('productos/productoTabla/:id','ProductController.destroy')//eliminar producto

//Clientes
Route.get('clientes','ClienteController.index')
Route.get('clientes/delete/:id','ClienteController.destroy')
Route.get('clientes/modificar/:id','ClienteController.viewModificar')
Route.post('clientes/modificar/:id','ClienteController.update')
Route.get('clientes/crear','ClienteController.mostrar')
Route.post('clientes','ClienteController.create')

// pedidos

Route.get('pedidos','PedidoController.index')
Route.get('pedidos/delete/:id','PedidoController.destroy')
Route.get('pedidos/modificar/:id','PedidoController.viewModificar')
Route.post('pedidos/modificar/:id','PedidoController.update')
Route.get('pedidos/crear','PedidoController.show')
Route.post('pedidos','PedidoController.create')