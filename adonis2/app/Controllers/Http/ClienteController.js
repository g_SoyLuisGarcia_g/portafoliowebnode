'use strict'
const client = use('App/Models/Client')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
class ClienteController {
    async index({ request, response, view }) {
        let clients = await client.all()
        return view.render('clientes/viewClientes', { clients: clients.rows })
    }

    async destroy({ params, request, response, view }) {
        const cl = await client.find(params.id);
        await cl.delete()
        return response.redirect('back')
    }

    async viewModificar({ params, view }) {
        let clients = await client.find(params.id)
        return view.render('clientes/modificar', { clients })
    }

    async update({ params, request, response, view }) {
        const clients = await client.find(params.id)
        clients.nombre = request.input('nombre')
        clients.apellido = request.input('apellido')
        clients.email = request.input('email')
        clients.telefono = request.input('telefono')
        await clients.save()
        return view.render('clientes/modificar', { clients })
    }

    async mostrar({ request, response, view }) {
        return view.render('clientes/crearCliente')
    }

    async create({ response, request, view }) {
        const nuevo = new client();
        nuevo.nombre = request.input('nombre')
        nuevo.apellido = request.input('apellido')
        nuevo.email = request.input('email')
        nuevo.telefono = request.input('telefono')
        await nuevo.save()
        return response.redirect('back')
    }
}

module.exports = ClienteController
