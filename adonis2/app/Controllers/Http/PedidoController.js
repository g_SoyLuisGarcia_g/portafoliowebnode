'use strict'
const pedidos = use('App/Models/Pedido')
const products = use('App/Models/Product')
const clients = use('App/Models/Client')
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with pedidos
 */
class PedidoController {
  /**
   * Show a list of all pedidos.
   * GET pedidos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  async index({ request, response, view }) {
    let pedido = await pedidos.query().with('functionProduct').with('functionClient').fetch()
    console.log(pedido.toJSON());
    return view.render('pedidos/viewPedidos', { pedido: pedido.toJSON() })
  }

  /**
   * Render a form to be used for creating a new pedido.
   * GET pedidos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {

    const add = new pedidos()
        add.fecha = request.input('fecha')
        add.producto_id = request.input('producto_id')
        add.cliente_id = request.input('cliente_id')
        add.cantidad = request.input('cantidad')
        add.total = request.input('total')
        add.entregado = request.input('entregado')
        await add.save()
        return response.redirect('pedidos')
  }

  /**
   * Create/save a new pedido.
   * POST pedidos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
  }

  /**
   * Display a single pedido.
   * GET pedidos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {

    let producto = await products.all()
        let cliente = await clients.all()
        console.log(producto.toJSON())
        return view.render('pedidos/crearPedido',{product: producto.toJSON(), client: cliente.toJSON()})
  }

  /**
   * Render a form to update an existing pedido.
   * GET pedidos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update pedido details.
   * PUT or PATCH pedidos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const pedido = await pedidos.find(params.id)
    pedido.fecha = request.input('fecha')
    pedido.producto_id = request.input('producto_id')
    pedido.cliente_id = request.input('cliente_id')
    pedido.cantidad = request.input('cantidad')
    pedido.total = request.input('total')
    pedido.entregado = request.input('entregado')
    await pedido.save()
    return response.redirect('back')
  }

  /**
   * Delete a pedido with id.
   * DELETE pedidos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const pedido = await pedidos.find(params.id);
    await pedido.delete()
    return response.redirect('back')
  }

  async viewModificar({ params, view }) {
    let pedido = await pedidos.find(params.id)
    return view.render('pedidos/modificarPedido', { pedido })
  }
}

module.exports = PedidoController
