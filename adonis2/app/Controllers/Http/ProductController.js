'use strict'
const product = use('App/Models/Product')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
class ProductController {
    async index({ request, response, view }) {
        let products = await product.all()
        console.log(products.rows);
        return view.render('productos/productoTabla', { products: products.rows })
    }

    async create({ response, request, view }) {
        const add = new product();
        add.nombre = request.input('nombre')
        add.precio = request.input('precio')
        add.existencia = request.input('existencia')
        await add.save()
        return response.redirect('productoTabla')
    }

    async mostrar({ request, response, view }) {
        return view.render('productos/crearProducto')
    }

    async store({ request, response }) {
    }

    async show({ params, request, response, view }) {
        const producto = await product.find(params.id);
        return view.render('product', { producto: producto })
    }

    async edit({ params, request, response, view }) {
        let product = await product.find(params.id)
        return view.render('productos.editar', { product })
        //el campo value en el input poner el product.nombre
    }

    async viewModificar({ params, view }) {
        let products = await product.find(params.id)
        return view.render('productos/modificarProducto', { products })
    }

    async update({ params, request, response, view }) {
        const products = await product.find(params.id)
        products.nombre = request.input('nombre')
        products.precio = request.input('precio')
        products.existencia = request.input('existencia')
        await products.save()
        return view.render('productos/modificarProducto', { products })
        
    }

    async destroy({ params, request, response, view }) {
        const producto = await product.find(params.id);
        await producto.delete()
        return response.redirect('back')
    }
}

module.exports = ProductController
