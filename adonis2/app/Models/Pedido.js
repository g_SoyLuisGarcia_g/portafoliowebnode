'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Pedido extends Model {
    static get table(){
        return 'pedidos'
    }

    functionProduct(){
        return this.belongsTo('App/Models/Product','producto_id')
    }

    functionClient(){
        return this.belongsTo('App/Models/Client','cliente_id')
    }
}

module.exports = Pedido
