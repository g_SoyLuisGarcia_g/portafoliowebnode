'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PortafolioSchema extends Schema {
  up () {
    this.create('portafolios', (table) => {
      table.increments()
      table.string('fotos',50)
      table.timestamps()
    })
  }

  down () {
    this.drop('portafolios')
  }
}

module.exports = PortafolioSchema
