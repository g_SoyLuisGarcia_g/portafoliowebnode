'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class InicioSchema extends Schema {
  up () {
    this.create('inicios', (table) => {
      table.increments()
        table.string ('opciones',50)
      table.timestamps()
    })
  }

  down () {
    this.drop ('inicios')
  }
}

module.exports = InicioSchema
