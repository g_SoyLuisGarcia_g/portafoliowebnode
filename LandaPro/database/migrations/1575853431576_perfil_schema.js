'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PerfilSchema extends Schema {
  up () {
    this.create('perfils', (table) => {
      table.increments()
      table.string ('foto',50 )
      table.string('nombre',50)
      table.string('texto',50)
      table.timestamps()
    })
  }

  down () {
    this.drop('perfils')
  }
}

module.exports = PerfilSchema
