'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Inicio extends Model {
    static get table(){
        return 'inicios'
    }
}

module.exports = Inicio
