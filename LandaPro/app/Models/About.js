'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class About extends Model {
    static get table(){
        return 'abouts'
    }
}

module.exports = About
