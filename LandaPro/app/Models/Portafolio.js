'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Portafolio extends Model {
    static get table(){
        return 'portafolios'
    }
}

module.exports = Portafolio
