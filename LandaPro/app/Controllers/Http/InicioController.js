'use strict'
const Inicio = use('App/Models/Inicio');
class InicioController {
    /**
     * Show a list of all abouts.
     * GET abouts
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     * @param {View} ctx.view
     */
    async index ({ request, response, view }) {
        let inicio = await Inicio.all();
        return response.json(inicio.rows);
      }
      /**
       * Render a form to be used for creating a new about.
       * GET abouts/create
       *
       * @param {object} ctx
       * @param {Request} ctx.request
       * @param {Response} ctx.response
       * @param {View} ctx.view
       */
      async create ({ request, response, view }) {
        const crear = new Inicio();
        crear.opciones = request.input('opciones');
        await crear.save();
        return response.json(crear);
      }
    
      /**
       * Create/save a new about.
       * POST abouts
       *
       * @param {object} ctx
       * @param {Request} ctx.request
       * @param {Response} ctx.response
       */
    
    
      /**
       * Display a single about.
       * GET abouts/:id
       *
       * @param {object} ctx
       * @param {Request} ctx.request
       * @param {Response} ctx.response
       * @param {View} ctx.view
       */
    
      /**
       * Render a form to update an existing about.
       * GET abouts/:id/edit
       *
       * @param {object} ctx
       * @param {Request} ctx.request
       * @param {Response} ctx.response
       * @param {View} ctx.view
       */
    
      /**
       * Update about details.
       * PUT or PATCH abouts/:id
       *
       * @param {object} ctx
       * @param {Request} ctx.request
       * @param {Response} ctx.response
       */
      async update ({ params, request, response }) {
        const inicio = await Inicio.find(params.id);
        inicio.opciones = request.input('opciones');
        await inicio.save();
        return response.json(inicio)
      }
    
      /**
       * Delete a about with id.
       * DELETE abouts/:id
       *
       * @param {object} ctx
       * @param {Request} ctx.request
       * @param {Response} ctx.response
       */
        async destroy ({ params, request, response }) {
        const inicio = await Inicio.find(params.id);
        await inicio.delete();
      }
}

module.exports = InicioController
