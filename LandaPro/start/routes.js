'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})
//inicio
Route.get('/inicio','InicioController.index');
Route.get('/inicio/crear','InicioController.create');
Route.get('/inicio/actualizar/:id','InicioController.update');
Route.get('/inicio/:id','InicioController.destroy');

//perfil
Route.get('/perfil','PerfilController.index');
Route.get('/perfil/crear','PerfilController.create');
Route.get('/perfil/actualizar/:id','PerfilController.update');
Route.get('/perfil/:id','PerfilController.destroy');
//portafolio

Route.get('/portafolio','PortafolioController.index');
Route.get('/portafolio/crear','PortafolioController.create');
Route.get('/portafolio/actualizar/:id','PortafolioController.update');
Route.get('/portafolio/:id','PortafolioController.destroy');

//titulo
Route.get('/titulo','TituloController.index');
Route.get('/titulo/crear','TituloController.create');
Route.get('/titulo/actualizar/:id','TituloController.update');
Route.get('/titulo/:id','TituloController.destroy');

//about
Route.get('about','aboutController.index');
Route.get('about/:id','aboutController.show');
Route.post('about/crear','aboutController.store');
Route.put('about/:id','aboutController.update');
Route.delete('about/:id','aboutController.destroy');

//contacto
Route.get('contacto','contactoController.index');
Route.get('contacto/:id','contactoController.show');
Route.post('contacto/crear','contactoController.store');
Route.put('contacto/:id','contactoController.update');
Route.delete('contacto/:id','contactoController.destroy');