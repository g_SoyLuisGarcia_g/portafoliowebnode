import React, {Component} from 'react';
import axios from 'axios';
const API_URL = "http//:127.0.0.1:3333";
class Contact extends Component {
    state = {
        contact:[]
    }
    componentDidMount(){
        const URL = `${API_URL}/contact`;
        axios.get(API_URL).then(Response =>Response.data)
        .then((data)=>{
            this.setState(({contact: data}))
        })
        .catch(e=>{console.log(e)})
    }
    render() {
        return (
                <section className="contact-section bg-black">
            <div className="container">

            <div class="row">





            <div class="col-md-4 mb-3 mb-md-0">
                <div class="card py-4 h-100">
                    <div class="card-body text-center">
                    <i class="fas fa-envelope text-primary mb-2"></i>
                    <h4 class="text-uppercase m-0">Github</h4>
                    <hr class="my-4"/>
                    <div class="small text-black-50">
                        <a href="#">https://github.com/SoyLuisGarcia?tab=repositories</a>
                    </div>
                    </div>
                </div>
                </div>

                <div class="col-md-4 mb-3 mb-md-0">
                <div class="card py-4 h-100">
                    <div class="card-body text-center">
                    <i class="fas fa-envelope text-primary mb-2"></i>
                    <h4 class="text-uppercase m-0">Email</h4>
                    <hr class="my-4"/>
                    <div class="small text-black-50">
                        <a href="#">luis.uzumaki76@gmail.com</a>
                    </div>
                    </div>
                </div>
                </div>
                    
                <div class="col-md-4 mb-3 mb-md-0">
                <div class="card py-4 h-100">
                    <div class="card-body text-center">
                    <i class="fas fa-envelope text-primary mb-2"></i>
                    <h4 class="text-uppercase m-0">Facebook</h4>
                    <hr class="my-4"/>
                    <div class="small text-black-50">
                        <a href="#">https://www.facebook.com/LuisGarciaGLee</a>
                    </div>
                    </div>
                </div>
                </div>
            </div>

            <div class="social d-flex justify-content-center">
                <a href="#" class="mx-2">
                <i class="fab fa-twitter"></i>
                </a>
                <a href="#" class="mx-2">
                <i class="fab fa-facebook-f"></i>
                </a>
                <a href="#" class="mx-2">
                <i class="fab fa-github"></i>
                </a>
            </div>

            </div>
        </section>
     
       );
    }
}
export default Contact;
