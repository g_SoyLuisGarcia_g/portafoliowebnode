import React, {Component} from 'react';
import axios from 'axios';
const API_URL = "http://127.0.0.1:3333";
class Navigation extends Component {
    state = {
        inicio : []
      }
    
      componentDidMount(){ 
        const URL = `${API_URL}/inicio`;
        axios.get(URL).then(Response => Response.data)
        .then((data) =>{
          this.setState({inicio: data})
          console.log(this.state.inicio);
        })
        .catch(e=>{
          console.log(e);
        })
      }

    render() {
        return (
                <nav className="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
                    <div class="container">
                        <a class="navbar-brand js-scroll-trigger" href="#page-top">Soy Luis</a>
                        <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                            Menu
                            <i class="fas fa-bars"></i>
                        </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                        {this.state.inicio.map((ciclo)=>
                          <li class="nav-item mx-0 mx-lg-1">
                            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href={'#'+ciclo.id}>{ciclo.opciones}</a>
                            </li>
                        )}
                    </ul>
                </div>
                </div>
                </nav>
           
            

        );
    }
}
export default Navigation;