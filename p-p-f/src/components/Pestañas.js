import React, {Component} from 'react';
import axios from 'axios';
const API_URL = "http://127.0.0.1:3333";

class Pestañas extends Component {
    
  state = {
    inicio : []
  }

  componentDidMount(){ 
    const URL = `${API_URL}/perfil`;
    axios.get(URL).then(Response => Response.data)
    .then((data) =>{
      this.setState({inicio: data})
      console.log(this.state.inicio);
    })
    .catch(e=>{
      console.log(e);
    })
  }


    render() {
        return (
            <React.Fragment>
                <header className="masthead bg-primary text-white text-center">
                {this.state.inicio.map((ciclo)=>
                  <div class="container d-flex align-items-center flex-column">     
                  
                <img className="masthead-avatar mb-5" src={require('./'+ciclo.foto)}/>             
    <h1 class="masthead-heading text-uppercase mb-0">{ciclo.nombre}</h1>
            
                 
                  <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon">
                      <i class="fas fa-star"></i>
                    </div>
                    <div class="divider-custom-line"></div>
                  </div>
            
                
                <p class="masthead-subheading font-weight-light mb-0">{ciclo.texto}</p>
            
                </div>
                )}
    
  </header>
            </React.Fragment>
               
            
        );
    }
}
export default Pestañas;