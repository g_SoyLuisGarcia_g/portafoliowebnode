import React, {Component} from 'react';
import axios from 'axios';
const API_URL = "http://127.0.0.1:3333";

class SectioAbout extends Component {
  state = {
    inicio : []
  }

  componentDidMount(){ 
    const URL = `${API_URL}/titulo`;
    axios.get(URL).then(Response => Response.data)
    .then((data) =>{
      this.setState({inicio: data})
      console.log(this.state.inicio);
    })
    .catch(e=>{
      console.log(e);
    })
  }
    render() {
        return (
            <section class="page-section bg-primary text-white mb-0" id="about">
            <div class="container">
        
              
              <h2 class="page-section-heading text-center text-uppercase text-white">Quien soy</h2>
        
             
              <div class="divider-custom divider-light">
                <div class="divider-custom-line"></div>
                <div class="divider-custom-icon">
                  <i class="fas fa-star"></i>
                </div>
                <div class="divider-custom-line"></div>
              </div>
        
             
              
              {this.state.inicio.map((ciclo)=>
              <center>
                <div class="col-lg-4 ml-2">
              <p class="lead"> {ciclo.titulo}</p>
                </div>
              </center>  
               
               
                )}
             
              
              
              
            </div>
          </section>

        );
    }
}
export default SectioAbout;